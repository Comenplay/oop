<?php

    require('Frog.php');
    require('Ape.php');
    // Release 1
    $sheep = new Animal("shaun");
    echo "<br> Name : ".$sheep->name; // "shaun"
    echo "<br> Legs : ".$sheep->legs; // 4
    echo "<br> Cold Blooded : ".$sheep->cold_blooded."<br>"; // "no"
    // Release 1
    $frog = new Frog("buduk");
    echo "<br> Name : ".$frog->name; // "buduk"
    echo "<br> Legs : ".$frog->legs; // 4
    echo "<br> Cold Blooded : ".$frog->cold_blooded."<br>"; // "no"
    echo $frog->jump(); //"Jump : Hop Hop"
    echo "<br>";
    $ape = new Ape("kera sakti");
    echo "<br> Name : ".$ape->name; // "kera sakti"
    echo "<br> Legs : ".$ape->legs; // 2
    echo "<br> Cold Blooded : ".$ape->cold_blooded."<br>"; // "no"
    echo $ape->yell();
?>